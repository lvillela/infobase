import { Injectable } from "@angular/core";
import { promise } from "protractor";

@Injectable({
    providedIn: 'root'
})
export class DialogService {

    confirm(message?: string) {
        return new Promise ( resolve => {
            return resolve(window.confirm(message || 'Confirm ?'));
        })
    }


}