import { User } from 'src/app/model/user.model';
import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../service/shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  shared : SharedService;
  user: User;

  constructor() { 
    this.user = new User();
  }

  ngOnInit() {
  }

}
