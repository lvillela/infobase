import { User } from './user.model';
export class Ticket {

    id: string;
    number: number;
    title: string;
    status: string;
    priority: string;
    imagem: string;
    user: User;
    assigned: User;
    data: string;
    changes: Array<string>;

    constructor(id?: string, number?: number, title?: string, status?: string, priority?: string, imagem?: string, user?: User, assigned?: User, data?: string) {
        this.id = id;
        this.number = number;
        this.title = title;
        this.status = status;
        this.priority = priority;
        this.imagem = imagem;
        this.user = user;
        this.assigned = assigned;
        this.data = data;
    }
}